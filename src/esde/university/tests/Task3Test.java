package esde.university.tests;

import esde.university.tasks.Task3;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class Task3Test {
    private static final String[] names = new String[]
            {"Egor", "Artiom", "Egor", "Vlad", "Igor", "Ivan", "Kirill"};
    private static final String[] surnames = new String[]
            {"Anufriev", "Abikenov", "Kalinchuk", "Ilyin", "Loginov", "Minin", "Fomichev"};
    private static final String[] expected_names = new String[]
            {"Artiom", "Egor", "Egor", "Igor", "Ivan", "Kirill", "Vlad"};
    private static final String[] expected_surnames = new String[]
            {"Abikenov", "Anufriev", "Kalinchuk", "Loginov", "Minin", "Fomichev", "Ilyin"};

    @Test
    public void bubbleSortTest () {
        String[][] result = Task3.bubbleSort(names, surnames);
        assertArrayEquals(expected_names, result[0]);
        assertArrayEquals(expected_surnames, result[1]);
    }


    @Test
    public void insertSortTest() {
        String[][] result = Task3.insertSort(names, surnames);
        assertArrayEquals(expected_names, result[0]);
        assertArrayEquals(expected_surnames, result[1]);
    }


    @Test
    public void directSelectionSortTest() {
        String[][] result = Task3.directSelectionSort(names, surnames);
        assertArrayEquals(expected_names, result[0]);
        assertArrayEquals(expected_surnames, result[1]);
    }
}
