package esde.university.tests;
import esde.university.tasks.Task1;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class Task1Test {
    private static final char[] case_one = new char[]{'a', 'b', 'd', 'c', '-', 'r', 'a', '7'};
    private static final char[] expected_one = new char[]{'-', '7', 'a', 'a', 'b', 'c', 'd', 'r'};

    @Test
    public void bubbleSortTest () {
        assertArrayEquals(expected_one, Task1.bubbleSort(case_one));
    }


    @Test
    public void insertSortTest() {
        assertArrayEquals(expected_one, Task1.insertSort(case_one));
    }


    @Test
    public void directSelectionSortTest() {
        assertArrayEquals(expected_one, Task1.directSelectionSort(case_one));
    }
}
