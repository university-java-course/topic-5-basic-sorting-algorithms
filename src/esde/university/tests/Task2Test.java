package esde.university.tests;

import esde.university.tasks.Task2;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class Task2Test {
    private static final String[] case_one = new String[]{"lol", "kek", "cheburek", "1234", "true", "Cheburek", "Task2", "task2"};
    private static final String[] expected_one = new String[]{"1234", "Cheburek", "Task2", "cheburek", "kek", "lol", "task2", "true"};

    @Test
    public void bubbleSortTest () {
        assertArrayEquals(expected_one, Task2.bubbleSort(case_one));
    }


    @Test
    public void insertSortTest() {
        assertArrayEquals(expected_one, Task2.insertSort(case_one));
    }


    @Test
    public void directSelectionSortTest() {
        assertArrayEquals(expected_one, Task2.directSelectionSort(case_one));
    }
}
