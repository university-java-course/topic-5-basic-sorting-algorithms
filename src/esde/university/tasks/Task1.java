package esde.university.tasks;

import java.util.Arrays;

public class Task1 {

    public static char[] bubbleSort(char[] array){
        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if(array[i] > array[j]){
                    _swap(i, j, array);
                }
            }
        }

        return array;
    }

    public static char[] insertSort(char[] array){
        for (int i = 1; i < array.length; i++) {
            for (int j = i - 1; j >= 0; j--) {
                if(array[i] >= array[j]){
                    break;
                }

                _swap(i, j, array);

                i--;
            }
        }

        return array;
    }


    public static char[] directSelectionSort(char[] array){
        for (int i = 0; i < array.length - 1; i++) {
            int minIndex = i;

            for (int j = i; j < array.length; j++) {
                if(array[minIndex] > array[j]) {
                    minIndex = j;
                }
            }

            if(i != minIndex){
                _swap(i, minIndex, array);
            }
        }
        return array;
    }

    private static void _swap(int i, int j, char[] array){
        char temp = array[i];
        array[i] = array[j];
        array[j] = temp;
        System.out.println(Arrays.toString(array));
    }
}
