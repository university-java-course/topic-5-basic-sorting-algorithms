package esde.university.tasks;

import java.util.Arrays;

public class Task2 {

    public static String[] bubbleSort(String[] array){
        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if(array[i].compareTo(array[j]) > 0){
                    _swap(i, j, array);
                }
            }
        }

        return array;
    }

    public static String[] insertSort(String[] array){
        for (int i = 1; i < array.length; i++) {
            for (int j = i - 1; j >= 0; j--) {
                if(array[i].compareTo(array[j]) >= 0){
                    break;
                }

                _swap(i, j, array);

                i--;
            }
        }

        return array;
    }


    public static String[] directSelectionSort(String[] array){
        for (int i = 0; i < array.length - 1; i++) {
            int minIndex = i;

            for (int j = i; j < array.length; j++) {
                if(array[minIndex].compareTo(array[j]) > 0) {
                    minIndex = j;
                }
            }

            if(i != minIndex){
                _swap(i, minIndex, array);
            }
        }
        return array;
    }

    private static void _swap(int i, int j, String[] array){
        String temp = array[i];
        array[i] = array[j];
        array[j] = temp;
        System.out.println(Arrays.toString(array));
    }
}
