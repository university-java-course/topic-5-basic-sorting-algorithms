package esde.university.tasks;

import java.util.Arrays;

public class Task3 {

    public static String[][] bubbleSort(String[] names, String[] surnames){
        for (int i = 0; i < names.length; i++) {
            for (int j = i + 1; j < names.length; j++) {
                if(_compare(i, j, names, surnames) > 0){
                    _swap(i, j, names, surnames);
                }
            }
        }

        return new String[][]{names, surnames};
    }

    public static String[][] insertSort(String[] names, String[] surnames){
        for (int i = 1; i < names.length; i++) {
            for (int j = i - 1; j >= 0; j--) {
                if(_compare(i, j, names, surnames) >= 0){
                    break;
                }

                _swap(i, j, names, surnames);

                i--;
            }
        }

        return new String[][]{names, surnames};
    }


    public static String[][] directSelectionSort(String[] names, String[] surnames){
        for (int i = 0; i < names.length - 1; i++) {
            int minIndex = i;

            for (int j = i; j < names.length; j++) {
                if(_compare(minIndex, j, names, surnames) > 0) {
                    minIndex = j;
                }
            }

            if(i != minIndex){
                _swap(i, minIndex, names, surnames);
            }
        }

        return new String[][]{names, surnames};
    }

    private static void _swap(int i, int j, String[] names, String[] surnames){
        String tempName = names[i];
        String tempSurname = surnames[i];

        names[i] = names[j];
        surnames[i] = surnames[j];

        names[j] = tempName;
        surnames[j] = tempSurname;
    }

    private static int _compare(int i, int j, String[] names, String[] surnames){
        if(names[i].compareTo(names[j]) > 0) return 1;
        if(names[i].compareTo(names[j]) < 0) return -1;
        if(surnames[i].compareTo(surnames[j]) > 0) return 1;
        if(surnames[i].compareTo(surnames[j]) < 0) return -1;
        return 0;
    }
}
